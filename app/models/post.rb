class Post < ActiveRecord::Base
	#dependent oznacza, że comments są zależne od danego Post'a. Zniszczenie Post'a ma powodować zniszczenie powiązanych z nim komentarzy
	has_many :comments, dependent: :destroy
	validates_presence_of :title
	validates_presence_of :body
end
